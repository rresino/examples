package com.examples.xml.parse.jdom.JDomParseExample;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class JDomParse {

	public static void main(String[] args) {

		try {
			Document doc = getDocument();

			Element root = doc.getRootElement();
			List<Element> elements = root.getChildren("dynamic-element");

			for (Element element : elements) {

				Element content = element.getChild("dynamic-content");
				CDATA text = new CDATA("aaaaaaaaaaa");
				content.setContent(text);

				break;
			}

			printDocument(doc);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static Document getDocument() throws ParserConfigurationException,
			IOException, JDOMException {
		SAXBuilder builder = new SAXBuilder();
		InputStream in = JDomParse.class.getClassLoader().getResourceAsStream(
				"field.xml");
		return builder.build(in);
	}

	private static void printDocument(Document doc)
			throws TransformerException, IOException {
		XMLOutputter xmlOutput = new XMLOutputter();
		System.out.println("-----------------------------------------------");
		// display nice nice
		xmlOutput.setFormat(Format.getPrettyFormat());
		xmlOutput.output(doc, System.out);
		System.out.println("-----------------------------------------------");
	}

}
