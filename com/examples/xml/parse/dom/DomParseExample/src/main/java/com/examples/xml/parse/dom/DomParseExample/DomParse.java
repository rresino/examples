package com.examples.xml.parse.dom.DomParseExample;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.CDATASection;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomParse {

	
	public static void main(String[] args) {
		
		try {
			Document doc = getDocument();
			Node root = doc.getFirstChild();
			
			Element element = (Element) doc.getElementsByTagName("dynamic-element").item(0);
			Element content = (Element) element.getElementsByTagName("dynamic-content").item(0);
			
			CharacterData cdataNode = (CharacterData)content.getFirstChild();
			cdataNode.setTextContent("my cdata content");
			printDocument(doc);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	
	private static void printDocument(Document doc) throws TransformerException {
		
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(System.out);
		System.out.println("-----------------------------------------------");
		transformer.transform(source, result);
		System.out.println("-----------------------------------------------");
	}
	
	private static Document getDocument() throws ParserConfigurationException, SAXException, IOException {
		InputStream in = DomParse.class.getClassLoader().getResourceAsStream(
				"field.xml");
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(in);
		
		return doc;		
	}
}
